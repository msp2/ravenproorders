function RavenLicenseCounter_2()
% Report details of Raven Pro orders in specified data range
%
% PREPARATION
%   1. Export a "RavenPro-distributed-keys-ONLY" report in Volusion.
%   2. Open in Excel and save in XLSX format. Do no modify!!!

% TO DO
% 1. Test that order is sorted

% Deb asked for 
%   Licensee details
%   Licensee institution
%   Licensee country
%   Person Billed
%   Institution Billed
%   Country Billed
%   Date of purchase
%   Amount Paid Total (calculate per order)
%

%---
% User input
%---
inFile = '.\input\RavenPro-distributed-keys-ONLY_20190801_123809.xlsx';
outFile = '.\output\RavenProOrders_20190101-20190731';
beginDate = 'Jan-1-2019';
endDate = 'Jul-31-2019';

%---
% Read export of Volusion "Raven Orders" report
%---
[~,~,Cin] = xlsread(inFile);
headerIn = Cin(1,:);
body = Cin(2:end,:);

%---
% Filter input
%---

% Filter by user-specified date range
orderdate = datenum(body(:,strcmp(headerIn,'orderdate')));
dateRange = [datenum(beginDate),datenum(endDate)];
body(orderdate<dateRange(1) | orderdate>dateRange(2),:) = [];

% Filter out canceled orders
pkey_used = body(:,strcmp(headerIn,'pkey_used'));
body(~strcmp(pkey_used,'Y'),:) = [];

% Filter out orders with no payment (likely due to discounts)
total_payment_received = cell2mat(body(:,strcmp(headerIn,'total_payment_received')));
body(total_payment_received==0,:) = [];

% Sort body by orderid
orderid = cell2mat(body(:,ismember('orderid',headerIn)));
if ~issorted(orderid)
    [orderid,sortIDX] = sort(orderid);
    body = body(sortIDX,:);
end

% Filter out redundant records when quantity > 1
numRecords = length(orderid);
quantity = cell2mat(body(:,strcmp(headerIn,'quantity')));
uniqueID = unique(orderid);
numOrders = length(uniqueID);
deleteFlag = false(numRecords,1);
for i = 1:numOrders
    idx = find(orderid==uniqueID(i));
    numItems = length(idx);
    j = 1;
    while j<=numItems
        qty = quantity(idx(j));
        if qty > 1
            deleteFlag(idx(2:end)) = true;
        end
        j = j + qty;
    end
end
body(deleteFlag,:) = [];

%---
% Output report
%---
headerOut = {
    'orderid'
    'orderdate'
    'total_payment_received'
    'billingcompanyname'
    'billingfirstname'
    'billinglastname'
    'billingaddress1'
    'billingaddress2'
    'billingcity'
    'billingstate'
    'billingpostalcode'
    'billingcountry'
    'billingphonenumber'
    'shipcompanyname'
    'shipfirstname'
    'shiplastname'
    'shipaddress1'
    'shipaddress2'
    'shipcity'
    'shipstate'
    'shippostalcode'
    'shipcountry'
    'shipphonenumber'
    'shipfaxnumber'
    'emailaddress'
    'customer_ipaddress'
};
[~,idx] = ismember(headerOut,headerIn);
bodyOut = body(:,idx);
Cout = [headerOut' ; bodyOut];
xlswrite(outFile,Cout)

disp(' ')
disp('Done processing.')